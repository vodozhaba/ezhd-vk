import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
import ezhd
import logging
import datetime
import os
import time

ezhd_email = os.environ['ezhd_email']
ezhd_password = os.environ['ezhd_password']
vk_login = os.environ['vk_login']
vk_password = os.environ['vk_password']

logging.basicConfig(level=logging.INFO)
ezhd = ezhd.Ezhd(ezhd_email, ezhd_password)

vk = vk_api.VkApi(login = vk_login, password=vk_password)
vk.auth()
lp = VkLongPoll(vk)

for event in lp.listen():
    if event.type == VkEventType.MESSAGE_NEW and event.text.lower() == u'дз':
        logging.info('Summoned')
        ezhd.auth()
        for i in range(1, 9):
            date = datetime.date.today() + datetime.timedelta(days=i)
            assignments = ezhd.student_homeworks(date)
            subjects_with_homework = set([assignment['homework']['subject']['name'] for assignment in assignments])
            schedule_items = ezhd.schedule_items(date)
            if schedule_items:
                subjects_without_homework = set([item['subject_name'] for item in schedule_items if not item['homeworks_to_verify'] and item['subject_name'] not in subjects_with_homework])
                message = u'Дзшечку на ' + date.strftime('%d.%m') + u' завезли\n\n'
                for subject in subjects_with_homework:
                    message += subject + u'\n'
                    homework = [assignment['description'] for assignment in assignments if assignment['homework']['subject']['name'] == subject]
                    message += u'\n'.join(homework) + '\n\n'
                if subjects_without_homework:
                    message += u', '.join(subjects_without_homework) + u' - не задано'
                message_id = vk.method(method = 'messages.send', values = {'peer_id': event.peer_id, 'message': message})
                vk.method('messages.pin', {'peer_id': event.peer_id, 'message_id': message_id})
                break
            elif i == 8:
                message_id = vk.method(method = 'messages.send', values = {'peer_id': event.peer_id, 'message': u'В ближайшее время нет занятий'})