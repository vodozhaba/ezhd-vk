from apscheduler.schedulers.blocking import BlockingScheduler
import datetime
import ezhd
import logging
import os

ezhd_email = os.environ['ezhd_email']
ezhd_password = os.environ['ezhd_password']

logging.basicConfig(level=logging.INFO)
ezhd = ezhd.Ezhd(ezhd_email, ezhd_password)

def duty_cycle():
    ezhd.auth()
    date = datetime.datetime.today() - datetime.timedelta(days = 7)
    logging.info(date)
    records = ezhd.aware_journals_info(date)
    if not ezhd.aware_journals_info(date):
        logging.info('no record, creating')
        ezhd.aware_journals_create(date)
    else:
        logging.info('found a record')

duty_cycle()

sched = BlockingScheduler()
sched.add_job(duty_cycle, 'cron', day_of_week='mon', hour='12')
sched.start()