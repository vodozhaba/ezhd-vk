
import requests
import itertools
import urllib.parse
import re
import json
import logging
import datetime

class Ezhd:

    _init_url = 'https://www.mos.ru/pgu/ru/application/dogm/journal/'
    _auth_url = 'https://login.mos.ru/sps/login/methods/password'
    _sessions_url = 'https://dnevnik.mos.ru/lms/api/sessions'
    _homework_url = 'https://dnevnik.mos.ru/core/api/student_homeworks'
    _student_profiles_url = 'https://dnevnik.mos.ru/core/api/student_profiles/%d'
    _schedule_url = 'https://dnevnik.mos.ru/jersey/api/schedule_items'
    _parent_student_profiles_url = 'https://dnevnik.mos.ru/core/api/student_profiles'
    _parent_aware_journals_url = 'https://dnevnik.mos.ru/core/api/aware_journals'

    _cookies = requests.cookies.RequestsCookieJar()


    def __init__(self, email, password):

        self._email = email
        self._password = password


    def _request(self, url, method = None, follow_redirects = True, **kwargs):

        if method is None:
            # deduce method from parameters
            if 'data' in kwargs or 'json' in kwargs:
                method = 'post'
            else:
                method = 'get'

        if method == 'get':
            resp = requests.get(url, allow_redirects = False, cookies=self._cookies, **kwargs)
        elif method == 'post':
            resp = requests.post(url, allow_redirects = False, cookies=self._cookies, **kwargs)
        else:
            raise ValueError('invalid method: ' + method)

        self._cookies.update(resp.cookies)

        # GET => receive redirect => repeat
        while (resp.status_code == 301 or resp.status_code == 302 or resp.status_code == 303) and follow_redirects:
            if resp.status_code == 303:
                url = urllib.parse.urljoin(url, resp.headers['Location'])
            else:
                url = urllib.parse.unquote(resp.headers['Location'])
            resp = requests.get(url, allow_redirects = False, cookies=self._cookies)
            self._cookies.update(resp.cookies)

        if resp.status_code == 401:
            if url == self._auth_url:
                logging.error('Invalid credentials for ' + self._email)
                raise RuntimeError('invalid credentials')
            else:
                # token expired
                logging.warning('401 unauthorized. Authorizing')
                self.auth()
                return self._request(url = url, method = method, follow_redirects = follow_redirects, **kwargs)
        if resp.status_code != 200:
            # unknown error
            raise RuntimeError('request failed')

        return resp


    @staticmethod
    def _academic_year_id(date):

        if(date.month > 8):
            academic_year = date.year
        else:
            academic_year = date.year - 1
        # academic year XXXX-09-01 to YYYY-05-31 is passed as XXXX minus 2012
        return academic_year - 2012

    
    @staticmethod
    def _week_str(date, fmt = '%d.%m.%Y'):

        begin_date = date - datetime.timedelta(days = date.weekday())
        end_date = begin_date + datetime.timedelta(days = 6)
        begin_date_str = begin_date.strftime(fmt)
        end_date_str = end_date.strftime(fmt)
        return (begin_date_str, end_date_str)


    def auth(self):

        self._cookies.clear()
        self._request(self._init_url)
        resp = self._request(self._auth_url, data = {'login': self._email, 'password': self._password, 'alien': False, 'bfp': '21b5bb75cc323f7e06ad31125a4a00b4'})
        # final location URL looks like https://.../?token=..., extract the token
        self._token = re.search('.*token=(.*)', resp.url).group(1)
        # get profile data
        resp = self._request(self._sessions_url, json = {'auth_token': self._token})
        self._data = json.loads(resp.text)
        logging.info('Logged in as ' + self._data['first_name'] + ' ' + self._data['last_name'])


    def student_homeworks(self, date, profile = 0):

        academic_year_id = self._academic_year_id(date)
        (begin_date_str, end_date_str) = self._week_str(date)
        pid = self._data['profiles'][profile]['id']
        
        # iterate through pages until reached an empty one
        page_num = 1
        data = []
        while True:
            resp = self._request(self._homework_url, headers = {'Profile-Id': str(pid), 'Auth-Token': self._token}, params = {
                'academic_year_id': academic_year_id,
                'begin_date': begin_date_str,
                'end_date': end_date_str,
                'page': page_num,
                'per_page': 50,
                'pid': pid,
                'student_profile_id': pid})
            page = json.loads(resp.text)
            if not page:
                break
            data += page
            page_num += 1
            
        # extract assignments with matching date
        date_str = date.strftime('%d.%m.%Y')
        assignments = [entry['homework_entry'] for entry in data if entry['homework_entry']['homework']['date_prepared_for'] == date_str]

        logging.info('Scraped homework assignments for ' + str(date))

        return assignments


    def student_profiles(self, date, profile = 0):

        academic_year_id = self._academic_year_id(date)

        # profile id
        pid = self._data['profiles'][profile]['id']

        # profile-specific URL
        url = self._student_profiles_url % pid

        resp = self._request(url = url, headers = {'Profile-Id': str(pid), 'Auth-Token': self._token}, params = {
            'academic_year_id': academic_year_id,
            'pid': pid,
            'with_groups': 'true',
            'with_ae_attendance': 'true',
            'with_archived_groups': 'true',
            'with_attendance': 'true',
            'with_ec_attendance': 'true',
            'with_lesson_info': 'true'})
        
        logging.info('Scraped profile data')

        return json.loads(resp.text)


    def schedule_items(self, date, profile = 0, groups = None):
        
        if groups is None:            
            # all available groups
            groups = self.student_profiles(date, profile)['groups']

        academic_year_id = self._academic_year_id(date)
        (begin_date, end_date) = self._week_str(date, '%Y-%m-%d')
        pid = self._data['profiles'][profile]['id']
        group_id = ', '.join([str(group['id']) for group in groups])
        
        resp = self._request(self._schedule_url, headers = {'Auth-Token': self._token, 'Profile-Id': str(pid)}, params = {
            'academic_year_id': academic_year_id,
            'from': begin_date,
            'group_id': group_id,
            'pid': pid,
            'student_profile_id': pid,
            'to': end_date
            })
        
        all_items = json.loads(resp.text)

        # extract items with matching date
        current = [item for item in all_items if tuple(item['date']) == (date.year, date.month, date.day)]

        logging.info('Scraped schedule items for ' + str(date))

        return current


    def parent_student_profiles(self, date, profile = 0):
        
        pid = self._data['profiles'][profile]['id']
        academic_year_id = self._academic_year_id(date)

        page_num = 1
        data = []
        while True:
            resp = self._request(self._parent_student_profiles_url, headers = {'Profile-Id': str(pid), 'Auth-Token': self._token}, params = {
                'academic_year_id': academic_year_id,
                'page': page_num,
                'per_page': 50,
                'pid': pid})
            page = json.loads(resp.text)
            if not page:
                break
            data += page
            page_num += 1

        return data


    def aware_journals_info(self, date, profile = 0, student_profile = 0):
        
        pid = self._data['profiles'][profile]['id']
        student_pid = self.parent_student_profiles(date, student_profile)[student_profile]['id']
        academic_year_id = self._academic_year_id(date)
        (begin_date, end_date) = self._week_str(date, '%Y-%m-%d')

        resp = self._request(self._parent_aware_journals_url, headers = {'Profile-Id': str(pid), 'Auth-Token': self._token}, params = {
            'pid': pid,
            'student_profile_ids': student_pid,
            'week_from': begin_date,
            'week_to': end_date
            })

        data = json.loads(resp.text)
        return data

    def aware_journals_create(self, date, profile = 0, student_profile = 0):
        
        pid = self._data['profiles'][profile]['id']
        student_pid = self.parent_student_profiles(date, student_profile)[student_profile]['id']
        (begin_date, _) = self._week_str(date, '%Y-%m-%d')

        resp = self._request(self._parent_aware_journals_url, headers = {'Profile-Id': str(pid), 'Auth-Token': self._token}, params = {'pid': pid}, json = {
            'parent_profile_id': pid,
            'student_profile_id': student_pid,
            'date': datetime.date.today().strftime('%Y-%m-%d'),
            'week': begin_date
            })
